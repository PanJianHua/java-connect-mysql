package com.panjianhua.learn.polymorphic.boy;

public class AnswersServer {
    public  Panjianhua answers(int type) {
        if(type == 0) {
            return new Character();
        }else if (type == 1) {
            return new Skill();
        }else {
            return new Hobby();
        }
    }
    public static void main(String[] args) {
        /*Panjianhua character = new Character();
        character.questions();
        Panjianhua skill = new Skill();
        skill.questions();
        Panjianhua hobby = new Hobby();
        hobby.questions();*/
        AnswersServer answersServer = new AnswersServer();
        Panjianhua panjianhua1 = answersServer.answers(1);
        panjianhua1.questions();


    }
}
