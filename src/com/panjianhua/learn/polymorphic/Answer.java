package com.panjianhua.learn.polymorphic;

import com.panjianhua.learn.polymorphic.boy.AnswersServer;
import com.panjianhua.learn.polymorphic.boy.Panjianhua;

public class Answer {
    public static void main(String[] args) {
        AnswersServer answersServer = new AnswersServer();
        Panjianhua panjianhua = answersServer.answers(0);
        panjianhua.questions();
    }
}
