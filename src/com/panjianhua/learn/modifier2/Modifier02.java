package com.panjianhua.learn.modifier2;
/*
* public static void main(String[] args)
该段程序为JAVA程序的入口，即主程序。
JVM执行的时候首先就是要找到main函数。

修饰符的详解：
pubic：表示该程序的访问权限，表示任何时候都可以被引用的时候使用public，JVM即可找到main函数来执行程序。
static：表示该函数是静态的，不依赖对象的，在类进行加载的时候主程序也会随着加载到内存当中去。
String[] args：从控制台接受参数。
void：不需要返回值
main：主函数
*
* */
public class Modifier02 {
    public static void main(String[] args) {

    }
}
