package com.panjianhua.learn.modifier2;

public class RichGeneration01 {
    protected String name;//富代姓名
    protected String moneyAmount;//账户总钱数
    protected String richList;//富豪排行榜
    protected long childrens;//孩子数量
    protected String wives;//老婆数量
    public void king() {
        System.out.println("不但是富豪一代，还是一朝皇帝，且史上都不敢记载");
    }

    // 组合输出
    public void combination() {
        System.out.println("富代姓名：" + name);
        System.out.println("账户总钱数：" + moneyAmount);
        System.out.println("富豪排行榜：" + richList);
        System.out.println("孩子数量：" + childrens);
        System.out.println("老婆数量：" + wives);
    }

    public static void main(String[] args) {
        RichGeneration01 richGeneration01 = new RichGeneration01();
        richGeneration01.name = "潘皇朝";
        richGeneration01.moneyAmount = "万亿吨黄金";
        richGeneration01.richList = "朝排行榜第一";
        richGeneration01.childrens = 66666;
        richGeneration01.wives = "10000";
        richGeneration01.combination();
        richGeneration01.king();
    }


}
