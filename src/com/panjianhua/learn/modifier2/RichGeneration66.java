package com.panjianhua.learn.modifier2;

//extends关键字用于类的继承
//extends关键字后面是父类，前面的是子类
//子类会继承父类(default)/proteoted/public修饰符成员变量与方法
public class RichGeneration66 extends RichGeneration01 {
    private  void first() {
        System.out.println("历史上没记载的第一位清官");
    }
    public static void main(String[] args) {
        RichGeneration66 richGeneration66 = new RichGeneration66();
        richGeneration66.name = "潘富豪六十六代";
        richGeneration66.moneyAmount = "万亿吨白银";
        richGeneration66.richList = "没历史记载的朝代，富豪榜并列第一";
        richGeneration66.childrens = 6666;
        richGeneration66.wives = "1000";
        richGeneration66.combination();
        richGeneration66.first();
    }
}
