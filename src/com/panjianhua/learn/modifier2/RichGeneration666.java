package com.panjianhua.learn.modifier2;

public class RichGeneration666 extends RichGeneration01 {
    private void firstIT() {
        System.out.println("潘氏家族第一位从事技术开发人员，技术还是渣得很，人更渣");
    }
    public static void main(String[] args) {
        RichGeneration666 richGeneration666 = new RichGeneration666();
        richGeneration666.name = "潘健华";
        richGeneration666.moneyAmount = "负债13万亿人民币，没错是万亿单位";
        richGeneration666.richList = "全球最特困第一位";
        richGeneration666.childrens = 0;
        richGeneration666.wives = "至今还是单身";
        richGeneration666.combination();
        richGeneration666.firstIT();
    }
}
