package com.panjianhua.learn.tryCatch;

import java.util.InputMismatchException;
import java.util.Scanner;

public class TryCatch {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            System.out.println("请输入第一个数:");
            int panJianhua1 = input.nextInt();
            System.out.println("请输入第二个数:");
            int panJianhua2 = input.nextInt();
            System.out.println("两数相除等于：" + panJianhua1 / panJianhua2);
        }catch (InputMismatchException e) {
            System.out.println("您应该输入整数！");
        }catch (ArithmeticException e) {
            System.out.println("除数不能为0！");
        }
        System.out.printf("程序结束啦！");
    }
}
