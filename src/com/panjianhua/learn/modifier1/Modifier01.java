package com.panjianhua.learn.modifier1;

public class Modifier01 {
    public String userName;
    private String city = "广州";
    private long id;

    public String getCity(){
        return this.city;
    }

    public long getId(){
        return this.id;
    }
    public void setId(long id) {
        this.id = id;
    }
}
