package com.panjianhua.learn.modifier1;

public class Modifier03 {
    public static void main(String[] args) {
        Modifier01 modifier01 = new Modifier01();
        modifier01.userName = "潘健华";
        String city = modifier01.getCity();
        modifier01.setId(666);
        long id = modifier01.getId();
        System.out.println(modifier01.userName);
        System.out.println(city);
        System.out.println(id);
    }
}
