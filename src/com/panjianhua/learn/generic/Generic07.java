package com.panjianhua.learn.generic;
/**
 * 泛型方法与可变参数
 *
 * */
public class Generic07 {
    public <T> void printMsg( T... args){
        for(T t : args){
            System.out.println("泛型测试t is " + t);
        }
    }
//    printMsg("111",222,"aaaa","2323.4",55.55);
}
