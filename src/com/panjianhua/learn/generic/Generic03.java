package com.panjianhua.learn.generic;
/*
* 泛型通配符
* */

// 同一种泛型可以对应多个版本（因为参数类型是不确定的），不同版本的泛型类实例是不兼容的。
/*类型通配符一般是使用？代替具体的类型实参，注意了，此处’？’是类型实参，而不是类型形参 。重要说三遍！此处’？’是类型实参，而不是类型形参 ！ 此处’？’是类型实参，而不是类型形参 ！再直白点的意思就是，此处的？和Number、String、Integer一样都是一种实际的类型，可以把？看成所有类型的父类。是一种真实的类型。
可以解决当具体类型不确定的时候，这个通配符就是 ?  ；当操作类型时，不需要使用类型的具体功能时，只使用Object类中的功能。那么可以用 ? 通配符来表未知类型。*/

public class Generic03 {
    public void showHandSome1(Generic01<Number> obj){
        System.out.println(obj.getHandSome());
    }

    public void showHandSome2(Generic01<?> obj){
        System.out.println(obj.getHandSome());
    }

    public static void main(String[] args) {
        Generic01<Integer> gInteger = new Generic01<Integer>(123);
        Generic01<Number> gNumber = new Generic01<Number>(456);
        Generic03 generic03 = new Generic03();
        generic03.showHandSome1(gNumber);
    }

}
