package com.panjianhua.learn.generic;
/*注意：
1.泛型的类型参数只能是类类型，不能是简单类型。
2.不能对确切的泛型类型使用instanceof操作。如下面的操作是非法的，编译时会出错。

if(ex_num instanceof Generic<Number>){
}*/

public class UseGeneric01 {
    public static void main(String[] args) {
        //泛型的类型参数只能是类类型（包括自定义类），不能是简单类型
        //传入的实参类型需与泛型的类型参数类型相同，即为Integer.
        Generic01<Integer> generic01 = new Generic01<Integer>(666);
        Integer name =  generic01.getHandSome();
        System.out.println(name);

        //传入的实参类型需与泛型的类型参数类型相同，即为String.
        Generic01<String> genericString = new Generic01<String>("潘健华");
        System.out.println("帅哥是" + genericString.getHandSome());
    }

}
