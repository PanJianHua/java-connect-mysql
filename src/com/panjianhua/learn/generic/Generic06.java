package com.panjianhua.learn.generic;
// - 泛型使用方式三：  泛型方法三
/**
 *  类中的泛型方法
 * */
//泛型方法可以出现杂任何地方和任何场景中使用。但是有一种情况是非常特殊的，当泛型方法出现在泛型类中时

/**
 *
 * Java中@Override的作用
 *
 * @Override是伪代码,表示重写。(当然不写@Override也可以)，不过写上有如下好处:
 * 1、可以当注释用,方便阅读；
 * 2、编译器可以给你验证@Override下面的方法名是否是你父类中所有的，如果没有则报错。例如，你如果没写@Override，而你下面的方法名又写错了，这时你的编译器是可以编译通过的，因为编译器以为这个方法是你的子类中自己增加的方法。
 * 举例：在重写父类的onCreate时，在方法前面加上@Override 系统可以帮你检查方法的正确性。
 * @Override
 * public void onCreate(Bundle savedInstanceState)
 *
 * {…….}
 * 这种写法是正确的，如果你写成：
 *
 * @Override
 * public void oncreate(Bundle savedInstanceState)
 * {…….}
 * 编译器会报如下错误：The method oncreate(Bundle) of type HelloWorld must override or implement a supertype method，以确保你正确重写onCreate方法（因为oncreate应该为onCreate）。
 *
 * 而如果你不加@Override，则编译器将不会检测出错误，而是会认为你为子类定义了一个新方法：oncreate
 *
 * 一、是什么
 * 一句话：它是表示重写的注解
 * @Override注解是伪代码，用于表示被标注的方法是一个重写方法。
 * 不写也完全可以，但强烈建议写上！
 *
 * 二、为什么用
 * 既然不写@Override也可以重写父类的方法，那为什么非要“多此一举”写上这个注解呢？
 * 使用@Override注解主要有两个好处：
 * 1）帮助自己检查是否正确的重写父类方法
 * 2）明显的提示看代码的人，这是重写的方法
 *
 * 三、举例说明
 * 1.当我们在子类中重写父类的方法时，如果不小心写错名字或者写错参数，编译器是不会报错的，因为他会认为你在子类扩展了父类的方法或者重载了父类的方法，这是符合语法规范的，但是这并不是我们想要的结果，所以会带来bug，但是如果你在方法上加了@Override注解，一旦你写错这个方法，系统会提示@override出错，这样在开发过程中就可以迅速知道我们写错了，加快开发速度与准确性。
 * 2.代码的可读性是非常重要的，我们写的代码很多时候会被别人看到或者修改，加上@Override注解会让看代码的人一眼明白，这个方法是重写了父类的方法，可读性更佳！
 *
 * */

public class Generic06 {
    class Fruit{
        @Override
        public String toString() {
            return "fruit";
        }
    }

    static class Apple {
        @Override
        public String toString() {
            return "apple";
        }
    }

    static class Person{
        @Override
        public String toString() {
            return "Person";
        }
    }

    static class GenerateTest<T>{
        public void show_1(T t){
            System.out.println(t.toString());
        }

        //在泛型类中声明了一个泛型方法，使用泛型E，这种泛型E可以为任意类型。可以类型与T相同，也可以不同。
        //由于泛型方法在声明的时候会声明泛型<E>，因此即使在泛型类中并未声明泛型，编译器也能够正确识别泛型方法中识别的泛型。
        public <E> void show_3(E t){
            System.out.println(t.toString());
        }

        //在泛型类中声明了一个泛型方法，使用泛型T，注意这个T是一种全新的类型，可以与泛型类中声明的T不是同一种类型。
        public <T> void show_2(T t){
            System.out.println(t.toString());
        }
    }

    public static void main(String[] args) {
        Apple apple = new Apple();
        Person person = new Person();

        GenerateTest<Fruit> generateTest = new GenerateTest<Fruit>();
        //apple是Fruit的子类，所以这里可以
//        generateTest.show_1(apple);
        //编译器会报错，因为泛型类型实参指定的是Fruit，而传入的实参类是Person
        //generateTest.show_1(person);

        //使用这两个方法都可以成功
        generateTest.show_2(apple);
        generateTest.show_2(person);

        //使用这两个方法也都可以成功
        generateTest.show_3(apple);
        generateTest.show_3(person);
    }
}
