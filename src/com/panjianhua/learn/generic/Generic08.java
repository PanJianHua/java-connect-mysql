package com.panjianhua.learn.generic;
/**
 * 静态方法与泛型
 * 静态方法有一种情况需要注意一下，那就是在类中的静态方法使用泛型：静态方法无法访问类上定义的泛型；如果静态方法操作的引用数据类型不确定的时候，必须要将泛型定义在方法上。
 *
 * 即：如果静态方法要使用泛型的话，必须将静态方法也定义成泛型方法 。
 *
 *
 * 泛型方法总结：
 *
 * 泛型方法能使方法独立于类而产生变化，以下是一个基本的指导原则：
 *
 * 无论何时，如果你能做到，你就该尽量使用泛型方法。也就是说，如果使用泛型方法将整个类泛型化，那么就应该使用泛型方法。另外对于一个static的方法而已，无法访问泛型类型的参数。所以如果static方法要使用泛型能力，就必须使其成为泛型方法。
 * */
public class Generic08<T> {
    /**
     * 如果在类中定义使用泛型的静态方法，需要添加额外的泛型声明（将这个方法定义成泛型方法）
     * 即使静态方法要使用泛型类中已经声明过的泛型也不可以。
     * 如：public static void show(T t){..},此时编译器会提示错误信息：
     "StaticGenerator cannot be refrenced from static context"
     */
    public static <T> void show(T t){

    }

    public static void main(String[] args) {
        int a = 2;
        int b = 999;
        int c = 3;
        System.out.println((b+Math.sqrt(b*b-4*a*c))/(2*a));
    }
}
