package com.panjianhua.learn.generic;

// - 泛型使用方式二：  泛型接口
//泛型接口与泛型类的定义及使用基本相同。泛型接口常被用在各种类的生产器中，可以看一个例子：
/*
* //定义一个泛型接口
public interface GeneratorInterface02<T> {
    public T next();
}
*
* */

//当实现泛型接口的类，未传入泛型实参时：
/**
 * 未传入泛型实参时，与泛型类的定义相同，在声明类的时候，需将泛型的声明也一起加到类中
 * 即：class FruitGenerator<T> implements Generator<T>{
 * 如果不声明泛型，如：class FruitGenerator implements Generator<T>，编译器会报错："Unknown class"
 */
public class Generic02_1<T> implements Generic02Interface<T> {
    @Override
    public T next() {
        return null;
    }
}
