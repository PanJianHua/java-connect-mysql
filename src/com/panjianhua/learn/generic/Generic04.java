package com.panjianhua.learn.generic;

import java.util.ArrayList;

// - 泛型使用方式三：  泛型方法
/*
* 在java中,泛型类的定义非常简单，但是泛型方法就比较复杂了。
尤其是我们见到的大多数泛型类中的成员方法也都使用了泛型，有的甚至泛型类中也包含着泛型方法，这样在初学者中非常容易将泛型方法理解错了。
泛型类，是在实例化类的时候指明泛型的具体类型；泛型方法，是在调用方法的时候指明泛型的具体类型 。
* */
public class Generic04 {
    /**
     * 泛型方法的基本介绍
     * @param tClass 传入的泛型实参
     * @return T 返回值为T类型
     * 说明：
     *     1）public 与 返回值中间<T>非常重要，可以理解为声明此方法为泛型方法。
     *     2）只有声明了<T>的方法才是泛型方法，泛型类中的使用了泛型的成员方法并不是泛型方法。
     *     3）<T>表明该方法将使用泛型类型T，此时才可以在方法中使用泛型类型T。
     *     4）与泛型类的定义一样，此处T可以随便写为任意标识，常见的如T、E、K、V等形式的参数常用于表示泛型。
     */
    public <T> T genericMethod(Class<T> tClass)throws InstantiationException , IllegalAccessException {
        T instance = tClass.newInstance();
        return instance;
    }

    private int i = 1;

    public static int hashCode(int i) {
        return i;
    }
    /** throws Exception
     * 1.(终极解释！！！)throws Exception放在方法后边，是throws Exception表示的是本方法不处理异常，交给被调用处处理(如果你不希望异常层层往上抛，你就要用throws Exception) ，而且被调用处必须处理。
     * 2、throw new Exception 表示人为的抛出一个异常，例如：
     * public boolean insert(News n) {
     * try{
     * .....
     * }catch{
     * throw new Exception("这是我自己抛出的一个异常，如果我看到此段信息表示我这个方法这儿出错了，给自己看的！");
     * }finally{
     * }
     * }
     * 3，首先方法后边加上throws Exception的作用是抛出异常。其中Exception可以理解为所有异常，也可以抛出指定异常。如果方法后边不加throws Exception，方法出了异常就会向上传递抛出(如果方法有调用者，那就交给调用者处理，如果调用者继续一层层抛出，最终交给虚拟机，虚拟机处理，整个程序会中断！ 如果在程序中捕获  还可以继续进行。)。
     * 4，如果有异常你不用层层向上抛出那么你就要用throws Exception，然后在调用时加上try catch语句处理...。。。如果有异常我一般选择这种处理方法。相比不用throws Exception，加上了throws Exception后，调用该方法时，必须加上try...catch才可以(你加上throw exception。调用的地方就必须try catch，不然编译都不过。。这样代码就更健壮了。)。
     * 相当于一种约束，如果不加throws Exception，在多个地方调用方法时，添加try...catch也可以，但是有时候就会忘记加try...catch。
     * 5，另外异常处理的原则是尽可能早的catch异常，正常的程序不应该写throws Exception。
     * 6，运行异常（继承RuntimeException）可以不捕获，向上抛，如果一直没有处理，则jvm会自动处理（停止线程，打印异常）。
     * ---非运行期异常，必须捕获或者在方法声明。
     * */
    /*public static void main(String[] args) throws Exception {
        Generic04 generic04 = new Generic04();
        System.out.println(hashCode(88));

//        generic04.genericMethod();
        Object obj = generic04.genericMethod(Class.forName("com.panjianhua.learn.generic.Generic04"));
        System.out.println(obj);
    }*/
    /** 或者 */
    public static void main(String[] args) {
        Generic04 generic04 = new Generic04();
        System.out.println(hashCode(88));
//        generic04.genericMethod();
        try {
            Object obj = generic04.genericMethod(Class.forName("com.panjianhua.learn.generic.Generic04"));
            System.out.println(obj);
        }catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /*
     *
     * throws
     * Exception:如果出现未知错误，会跑出Exception。如果这里加了异常捕捉，调用test方法时，就要加上try...catch
     */
    private static void test() throws Exception {
        ArrayList list = new ArrayList();
        int x = 1;
        int y = 2;
        int z = 3;
        if (x + y >= z) {
            System.out.printf("逻辑正确！");
        } else {
            throw new Exception("test方法有问题"); // 手动抛出一个异常
        }
    }


}
