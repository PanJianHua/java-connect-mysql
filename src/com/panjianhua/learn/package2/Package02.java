package com.panjianhua.learn.package2;

import com.panjianhua.learn.package1.Package01;
//package 与 import区别： 不同包必须import,同包无需再引入，比如com.panjianhua.learn.package2包里的Package03

public class Package02 {
    public static void main(String[] args) {
        Package01 package01 = new Package01();
    }
}
