package com.panjianhua.learn.arrayList;

import java.util.ArrayList;
import java.util.List;
/*
* 1、向列表添加数据
对象名.add();
对象名.add(索引位置,数据)// 像当前数组尾部添加数据
2、泛型：<>
List <数据类型> 对象名 = new ArrayList<数据类型>();
3、得到指定位置数据
对象名.get(索引位置);
4、获得列表的数值总数
对象名.size();
5、移除数据
对象名.remove(索引位置);
6、移除最后一项数据
对象名.remove(对象名.size()-1);
7、遍历列表所有数据
For( 数据类型 变量名 ：列表名){
    //循环体
}
* 
* */

public class ArrayList01 {
    public static void main(String[] args) {
        List<String> name = new ArrayList<String>();
        name.add("潘老师");
        name.add("潘教授");
        name.add("潘帅");
        name.add("潘six666");
        name.add("潘金莲");
        name.add(0,"潘小哥");
        System.out.println(name);
        System.out.println(name.size());
        name.remove(4);
        System.out.println(name);
        System.out.println(name.size());
        name.remove(name.size() - 1);
        System.out.println(name);
        System.out.println(name.size());
        for (String key : name) {
            System.out.println(key);
        }
        for (Object key : name) {
            System.out.println("Object：" + key);
        }
    }
}
