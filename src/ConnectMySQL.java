import java.sql.*;
import java.util.Scanner;

public class ConnectMySQL {

    public static void main(String[] args) throws Exception {
        // 1.加载数据访问驱动
//        Class.forName("com.mysql.jdbc.Driver");
        Class.forName("com.mysql.cj.jdbc.Driver");
        //2.连接到数据"库"上去
        Connection conn= DriverManager.getConnection("jdbc:mysql://localhost:3306/reaction?characterEncoding=GBK", "root", "pan666six");
        //3.构建SQL命令
        Statement state=conn.createStatement();
//        String s="insert into articles values('008','李丽','四中')";
//        state.executeUpdate(s);
        /* SELECT * FROM `reaction`.`articles` LIMIT 0,1000 */
        String s="SELECT * FROM `reaction`.`articles` LIMIT 0,1000";
//        state.execute(s);
        ResultSet rs=state.executeQuery(s);//查询
        //将查询出的结果输出
        System.out.println(rs.next());
        while (rs.next()) {
            String title=rs.getString("title");
//            下列与上面语句作用相同，仅需一个就行
//            String deptno=rs.getString(1);
            System.out.println("title:"+title);
        }
        Scanner sc = new Scanner(System.in);
        int i = sc.nextInt();
//        System.out.println(i);
        //获取键盘录入数据
		System.out.println("输入");
		int a = sc.nextInt();
		System.out.println("我问问");
		int b = sc.nextInt();
    }

}