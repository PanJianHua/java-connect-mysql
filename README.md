# java-connect-mysql

### Java干货
* [GitHub 上可供新手阅读和玩耍的 Java 项目有哪些？](https://www.zhihu.com/question/34544815)
* [27天成为Java大神](https://github.com/DuGuQiuBai/Java)
* [Java3y](https://github.com/ZhongFuCheng3y/3y)
* [IntelliJ IDEA java项目导入jar包，打jar包](https://www.cnblogs.com/yulia/p/6824058.html)

### 文件结构
```
└─src
    │  ConnectMySQL.java
    │  Method.java
    │  test.java
    └─com
        └─panjianhua
            └─learn
                ├─arrayList
                │      ArrayList01.java ArrayList代替数组
                │
                ├─generic 泛型
                │      Generic01.java
                │      Generic02Interface.java
                │      Generic02_1.java
                │      Generic02_2.java
                │      Generic03.java
                │      Generic04.java
                │      Generic05.java
                │      Generic06.java
                │      Generic07.java
                │      Generic08.java
                │      UseGeneric01.java
                │
                ├─modifier1 修饰符1
                │      Modifier01.java
                │      Modifier03.java
                │
                ├─modifier2 修饰符2 继承
                │      Modifier02.java
                │      RichGeneration01.java
                │      RichGeneration66.java
                │      RichGeneration666.java
                │
                ├─package1 包1
                │      Package01.java
                │      Package03.java
                │
                ├─package2 包2
                │      Package02.java
                │
                └─polymorphic 多态
                    │  Answer.java
                    │
                    └─boy 多态包
                        AnswersServer.java
                        Character.java
                        Hobby.java
                        Panjianhua.java
                        Skill.java             
```

### java重点 -- 泛型
* [java 泛型详解-绝对是对泛型方法讲解最详细的，没有之一](https://blog.csdn.net/s10461/article/details/53941091) [或](https://www.cnblogs.com/coprince/p/8603492.html)
* [深入理解Java泛型](https://www.sohu.com/a/245549100_796914)
* [java 泛型基础问题汇总](https://www.cnblogs.com/huansky/p/8043149.html)
* [Java泛型的使用-泛型类，泛型方法，通配符](https://blog.csdn.net/fragrant_no1/article/details/84951350)
* [java泛型方法的使用（二）](https://blog.csdn.net/findmyself_for_world/article/details/42522811)
* [Java的throws Exception](https://blog.csdn.net/hanxiaoyong_/article/details/89431732)
* [三分钟学习Java泛型中T、E、K、V、？的含义](https://blog.csdn.net/u010648555/article/details/103858693)

### java重点
* [javase知识点大全总结](https://blog.csdn.net/qq_35336425/article/details/82825542)

### java框架 
* 原话源于[现在常用的java框架有哪些?](https://www.zhihu.com/question/362802033)

Java程序员必备的15个框架，学会这些20K+ 不是问题Java 程序员方向太多，且不说移动开发、大数据、区块链、人工智能这些，大部分 Java 程序员都是 Java Web/后端开发。  
那作为一名 Java Web 开发程序员必须需要熟悉哪些框架呢?今天，给大家列举了一些通用的、必须掌握的框架，学会这些，20K+ 不是问题。  
* 1.Spring。毫无疑问，Spring 框架现在是 Java 后端框架家族里面最强大的一个，其拥有 IOC 和 AOP 两大利器，大大简化了软件开发复杂性。并且，Spring 现在能与所有主流开发框架集成，可谓是一个万能框架，Spring 让 JAVA 开发变得更多简单。  
* 2.Spring MVC。Spring MVC 是一个 MVC 开源框架，用来代替 Struts。它是 Spring 项目里面的一个重要组成部分，能与 Spring IOC 容器紧密结合，以及拥有松耦合、方便配置、代码分离等特点，让 JAVA 程序员开发 WEB 项目变得更加容易。  
* 3.Spring Boot。Spring Boot 是 Spring 开源组织下的一个子项目，也是 Spring 组件一站式解决方案，主要是为了简化使用 Spring 框架的难度，简省繁重的配置。Spring Boot提供了各种组件的启动器(starters)，开发者只要能配置好对应组件参数，Spring Boot 就会自动配置，让开发者能快速搭建依赖于 Spring 组件的 Java 项目。  
* 4.Spring Cloud。Spring Cloud 是一系列框架的有序集合，是目前最火热的微服务框架首选，它利用Spring Boot 的开发便利性巧妙地简化了分布式系统基础设施的开发，如服务发现注册、配置中心、消息总线、负载均衡、断路器、数据监控等，都可以用 Spring Boot 的开发风格做到一键启动和部署。  
* 5.Mybatis/ iBatisiBatis 曾是开源软件组 Apache 推出的一种轻量级的对象关系映射持久层(ORM)框架，随着开发团队转投Google Code 旗下，ibatis 3.x 正式更名为 Mybatis，即：iBatis 2.x, MyBatis 3.x。  
* 6.Hibernate。Hibernate 是一个开放源代码的对象关系映射框架，它对 JDBC 进行了非常轻量级的对象封装，它将 POJO 与数据库表建立映射关系，是一个全自动的 orm 框架。Hibernate 可以自动生成 SQL 语句，自动执行，使得 Java 程序员可以随心所欲的使用对象编程思维来操作数据库。  
* 7.Dubbo Dubbo是阿里巴巴开源的基于 Java 的高性能 RPC 分布式服务框架，现已成为 Apache 基金会孵化项目。使用 Dubbo 可以将核心业务抽取出来，作为独立的服务，逐渐形成稳定的服务中心，可用于提高业务复用灵活扩展，使前端应用能更快速的响应多变的市场需求。  
* 8.NettyNetty。 是由 JBOSS 提供的一个开源的、异步的、基于事件驱动的网络通信框架，用 Netty 可以快速开发高性能、高可靠性的网络服务器和客户端程序，Netty 简化了网络应用的编程开发过程，使开发网络编程变得异常简单。  
* 9.ShiroApache。 Shiro是一个强大而灵活的开源安全框架，它干净利落地处理身份认证，授权，企业会话管理和加密。  
* 10.Ehcache。EhCache 是一个纯Java的进程内缓存框架，具有快速、精干等特点，是 Hibernate 中默认的CacheProvider。它使用的是 JVM 的堆内存，超过内存可以设置缓存到磁盘，企业版的可以使用 JVM 堆外的物理内存。  
* 11.Quartz。Quartz 是一个基于 Java 的广泛使用的开源的任务调度框架，做过定时任务的没有没用过这个框架的吧?  
* 12.Velocity。 Velocity 是一个基于 Java 的模板引擎，简单而强大的模板语言为各种 Web 框架提供模板服务，来适配 MVC 模型。  
* 13.jQuery。jQuery是一个快速、简洁的 JavaScript 框架，它封装 JavaScript 常用的功能代码，提供一种简便的 JavaScript 设计模式，极大地简化了 JavaScript 编程。虽然哥好久没做 Web 开发了，但哥也不曾忘记，也还记得一些常用的写法，如：$("#wx").html("javastack");  
* 14.JUnit。JUnit 是一个 Java 语言的单元测试框架，绝大多数 Java 的开发环境都已经集成了 JUnit 作为其单元测试的工具。  
* 15.Log4j。Log4j 是 Apache 的一个开源日志框架，通过 Log4j 我们可以将程序中的日志信息输出到控制台、文件等来记录日志。作为一个最老牌的日志框架，它现在的主流版本是 Log4j2。Log4j2是重新架构的一款日志框架，抛弃了之前 Log4j 的不足，以及吸取了优秀日志框架 Logback 的设计。如果上面的大部分没用过，甚至都没听说过，那就怀疑你是不是个假程序员了，要加油了。这些都是 Java 程序员必备的开发框架，有些不一定是首选的选择，但这些一定是 Java 程序员必备的。

* Servlet技术只是一些底层的技术。实际开发之中是不会用到的！  
  现在目前实际用到的开发技术：早在15年左右的时候：SSH；现在已经离开市场。  
  SpringData JPA前身就是hibernate！技术;  
  第一种技术：Spring+springMVC+mybatis; 都快淘汰了！  
  第二种技术： 这个技术是最主流的之一！前端：layui+thymeleaf后端：spring boot+mybatis  
  第三种技术：  
    前后端分离  
  前端：vue+elementUI+axios  
  后端：springboot+springcloud(微服务)+分布式架构+mybatis（或者JPA）;  
  在互联网公司，必知必会！;  
  学习java分为四个阶段：  
  1：javaSE  
  2: javaWEB （jsp，servlet,）  
  3:企业级框架：spring ，springMVC+mybatis，JPA，shiro，quartz，redis。。。。  
  4: 分布式+微服务：springcloud，dubbo+zookeeper，linux, MQ,。。。。
  
### idea编译报错处理方案
* [internal java compiler error](https://www.cnblogs.com/warrior4236/p/12408166.html)
* [Intellj idea快速创建某个接口的实现类](https://blog.csdn.net/My_name_is_ZwZ/article/details/82915407)

## SSM框架、Spring Boot、Spring Cloud
* Spring、Spring MVC、MyBatis 就是江湖大名鼎鼎的 SSM 框架  
* [Spring历史演变以及版本种类](https://www.jianshu.com/p/9f45cce88b0b)
* SSM = Spring + SpringMVC + MyBatis
后面就有了 SSM，但其实现在的新项目中用的也比较少了，而更多的用 Spring Boot。
* Spring Boot   
Spring Boot 确实配起来非常简单，省略了很多工作，配置文件不是那么多，更多的是用的注解的方式。
* Spring响应式微服务：Spring Boot 2+Spring 5+Spring Cloud实战

###  Maven
* [IDEA 如何搭建maven 安装、下载、配置（图文）](https://www.cnblogs.com/xihehua/p/9639045.html) 
* [idea中Maven中mysql-connector-java not found 或者其他依赖包下载不了的问题的解决办法](https://blog.csdn.net/weixin_42628809/article/details/106319555)  
* [maven(一) maven到底是个啥玩意~](https://www.cnblogs.com/whgk/p/7112560.html)

### Spring
* [Spring 教程](https://www.w3cschool.cn/wkspring/)
* [idea 新手创建Spring项目](https://blog.csdn.net/jiahanghacker/article/details/88871207)
* [Spring Framework 5 中文文档](https://lfvepclr.gitbooks.io/spring-framework-5-doc-cn/content/)

### Maven + SSM框架案列
* [Maven+SSM框架项目实例——IDEA](https://www.cnblogs.com/chedahui/p/10033607.html)
* [手把手整合SSM框架(示例代码)](https://www.136.la/shida/show-145398.html)
* [IDEA使用maven搭建SSM框架整合项目（超级详细，值得一看）](https://www.cnblogs.com/whtbky/p/13681272.html)
* [SSM框架：详细整合教程](https://www.flyzy2005.cn/tech/programmers/build-ssm/)
* [oa企业报销系统](https://gitee.com/xn2001/oa)

### Spring Boot
* [使用IDEA搭建一个简单的SpringBoot项目——详细过程](https://blog.csdn.net/baidu_39298625/article/details/98102453)

### 优秀博主
* [有梦想的老王](https://www.cnblogs.com/whgk/)
* [程序猿DD](https://blog.didispace.com/)
* [廖雪峰--Java教程](https://www.liaoxuefeng.com/wiki/1252599548343744)

## 数据分析【未来转行非程序员的一种选择】
* [如何快速成为数据分析师？](https://www.zhihu.com/question/29265587)
* 掌握Java
* 掌握MySQL
   -- where / group by / order by / left join / right join / inner join / null / not null / having / distinct / like / union / avg / sum / min / max
* 掌握Excel
* Python或者R的基础
  -- python科学计算包：pandas、numpy、scipy、scikit-learn
* Word
* Excel
* ppt
* 统计学
* 数据可视化：seaborn、matplotlib
* spark、hive
* 思维导图、mindmanager软件
* Hadoop
* linux

(分析软件主要推荐：  
* SPSS系列：老牌的统计分析软件，SPSS Statistics（偏统计功能、市场研究）、SPSS Modeler（偏数据挖掘）,不用编程，易学。
* SAS：老牌经典挖掘软件，需要编程。)

## 常用框架：
* 集成开发工具（IDE）：Eclipse、MyEclipse、Spring Tool Suite（STS）、Intellij IDEA、NetBeans、JBuilder、JCreator
* JAVA服务器：tomcat、jboss、websphere、weblogic、resin、jetty、apusic、apache
* 负载均衡：nginx、lvs
* web层框架：Spring MVC、Struts2、Struts1、Google Web Toolkit（GWT）、JQWEB
* 服务层框架：Spring、EJB
* 持久层框架：Hibernate、MyBatis、JPA、TopLink
* 数据库：Oracle、MySql、MSSQL、Redis
* 项目构建：maven、ant
* 持续集成：Jenkins
* 版本控制：SVN、CVS、VSS、GIT
* 私服：Nexus
* 消息组件：IBM MQ、RabbitMQ、ActiveMQ、RocketMq
* 日志框架：Commons Logging、log4j 、slf4j、IOC
* 缓存框架：memcache、redis、ehcache、jboss cache
* RPC框架：Hessian、Dubbo
* 规则引擎：Drools
* 工作流：Activiti
* 批处理：Spring Batch
* 通用查询框架：Query DSL
* JAVA安全框架：shiro、Spring Security
* 代码静态检查工具：FindBugs、PMD
* Linux操作系统：CentOS、Ubuntu、SUSE Linux、
* 常用工具：PLSQL Developer（Oracle）、Navicat（MySql）、FileZilla（FTP）、Xshell（SSH）、putty（SSH）、SecureCRT（SSH）、jd-gui（反编译）

### 如何学习一个框架或者技术：
* 是什么，简介，概述
* 有什么用，用途，使用场景
* 怎么用，在实际开发中的应用，注意事项
* 优缺点
* 框架原理，工作流程，工作原理
* 常见面试题
* 源码分析，核心类，核心方法，设计模式
* 发布博客，在开发和实践中，博客反馈中持续改进
* 与同事朋友交流，技术论坛，技术分享中持续丰富知识

## 框架选择后续
* SSM在前后端分离的背景下，已经过时
* 本人推荐Java框架：微服务架构 Spring 5 + Spring Boot 2 + Spring Cloud
* [5款Java微服务开源框架](https://blog.csdn.net/weixin_44571427/article/details/86546185)